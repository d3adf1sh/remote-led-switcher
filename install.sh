#!/bin/bash

while getopts "P:B" flag
do
  case "${flag}" in
    P) devicePort=${OPTARG};;
    B) bundle=true;;
  esac
done

if [ -z "$devicePort" ]; then
  echo "Device port [-P] not specified."
  exit 1
fi

if [ "$bundle" = true ]; then
  ./bundle.sh
fi

echo "Copying files to $devicePort."
declare -a fileNames=(
  settings.json
  src/lib
  src/device
  src/services
  src/apps
  src/server.py
  src/main.py
)

for i in "${fileNames[@]}"
do
   echo "Copying $i..."
   ampy --port $devicePort put $i
done

echo "All files copied."
