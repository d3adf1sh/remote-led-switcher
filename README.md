# Remote LED Switcher

Aplicação web com API REST para execução em microcontroladores.

O objetivo é disponibilizar uma interface web para ligar e desligar o LED da placa e notificar a operação através de
mensageria com o protocolo MQTT.

Back-end em Python com Microdot e front-end em NodeJS com React.

O front-end fica na pasta `webapp` e precisa ter o bundle gerado na pasta `src/apps/public`.

## Requisitos

- Um microcontrolador com rede sem fio e [MicroPython](https://micropython.org/);
- Um PC com Linux, NodeJS e Python;
- [Ampy](https://pypi.org/project/adafruit-ampy/) para interagir com o microcontrolador.

## Execução

Criar o arquivo `settings.json` com a configuração da rede sem fio, do serviço MQTT e do servidor embarcado.

```json
{
  "wifi": {
    "ssid": "ssid",
    "password": "password",
    "timeout": 10
  },
  "mqtt": {
    "server": "server",
    "port": 8883,
    "user": "user",
    "password": "password",
    "ssl": true
  },
  "server": {
    "port": 8282
  }
}
```

Instalar os módulos do NodeJS no front-end:

```bash
npm --prefix webapp install
```

Gerar o bundle do front-end na pasta `src/apps/public`:

```bash
./bundle.sh
```

Copiar a aplicação para o microcontrolador:

```bash
./install.sh -P {porta}
```

O script pode eventualmente gerar o bundle do front-end através do argumento `-B`.

Executar a aplicação:

```bash
./run.sh -P {porta}
```

O script pode eventualmente gerar o bundle do front-end e copiar a aplicação para o microcontrolador através dos
argumentos `-B` e `-I`.

Depois de executar, a aplicação mostrará o status da rede sem fio, do serviço MQTT e a URL na qual o servidor está
rodando:

```
Connected to wifi...
Connected to mqtt service...
Server running on http://192.168.15.160:8282.
```

![Aplicação](./samples/application.png)
