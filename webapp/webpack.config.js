const path = require('path');
const Webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, '../src/apps/public'),
    filename: 'index.js'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.(css|scss)$/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader'
        ]
      }    ]
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      '@services': path.resolve(__dirname, 'src/services/'),
      '@pages': path.resolve(__dirname, 'src/pages/'),
      '@styles': path.resolve(__dirname, 'src/styles/')
    }
  },
  plugins: [
    new Webpack.ProvidePlugin({
      'React': 'react'
    }),
    new HtmlWebpackPlugin({
        template: './public/index.html',
        filename: './index.html'
    })
  ],
  mode: 'development',
  devServer: {
    static: path.resolve(__dirname, '../src/apps/public'),
    compress: true,
    port: 3000,
    historyApiFallback: true
  }
}
