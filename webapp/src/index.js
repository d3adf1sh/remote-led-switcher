import ReactDOM from 'react-dom'
import Led from '@pages/led'
import '@styles/index.scss'

ReactDOM.render(<Led/>, document.getElementById('root'))
