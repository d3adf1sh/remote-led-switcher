import axios from 'axios'
import { checkFailure } from './request'

export const getStatus = (onSuccess, onFailure) => {
  axios.get('led').then(response => {
    onSuccess(response.data)
  }).catch(error => checkFailure(error, onFailure))
}

export const on = (onSuccess, onFailure) => {
  axios.post('led/on').then(response => {
    onSuccess(response.data)
  }).catch(error => checkFailure(error, onFailure))
}

export const off = (onSuccess, onFailure) => {
  axios.post('led/off').then(response => {
    onSuccess(response.data)
  }).catch(error => checkFailure(error, onFailure))
}
