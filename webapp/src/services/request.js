export const checkFailure = (error, onFailure) => {
  console.log(error)
  if (onFailure) {
    onFailure(error)
  }
}
