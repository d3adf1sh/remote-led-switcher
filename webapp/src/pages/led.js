import { useState, useEffect } from 'react'
import { getStatus, on, off } from '@services/led'
import '@styles/led.scss'

const Led = () => {
	const [ligado, setLigado] = useState(0)
	useEffect(() => {
		getStatus(data => setLigado(data.status))
  }, [])

	return (
		<div className='led'>
			<h2 className='header'>Remote LED Switcher</h2>
			<button
				className='action'
				disabled={ligado == 1}
				onClick={() => on(data => setLigado(data.status))}
			>
				Ligar
			</button>&nbsp;
			<button
				className='action'
				disabled={ligado == 0}
				onClick={() => off(data => setLigado(data.status))}
			>
				Desligar
			</button>
			<p className='text'>
				{`O LED está ${ligado > 0 ? 'ligado' : 'desligado'}.`}
			</p>
		</div>
	);
}

export default Led
