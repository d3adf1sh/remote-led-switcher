#!/bin/bash

while getopts "P:IB" flag
do
  case "${flag}" in
    P) devicePort=${OPTARG};;
    I) install=true;;
    B) bundle="-B";;
  esac
done

if [ -z "$devicePort" ]; then
  echo "Device port [-P] not specified."
  exit 1
fi

if [ "$install" = true ]; then
  ./install.sh -P $devicePort $bundle
fi

ampy --port $devicePort run src/main.py
