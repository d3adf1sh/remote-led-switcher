from lib.mqtt.robust import MQTTClient
from lib.mqtt.simple import MQTTException

_client = None


def connect(server, port, user=None, password=None, ssl=False):
    global _client
    if not _client:
        if ssl:
            ssl_params = {'server_hostname': server}
        _client = MQTTClient(
            client_id="remoteLEDSwitcher",
            server=server,
            port=port,
            user=user,
            password=password,
            ssl=ssl,
            ssl_params=ssl_params
        )

        try:
            _client.connect()
        except (OSError, MQTTException):
            _client = None
        return _client is not None
    else:
        return False


def publish(message, data):
    global _client
    if _client:
        _client.publish(message, data)
        return True
    else:
        return False


def disconnect():
    global _client
    if _client:
        try:
            _client.disconnect()
        finally:
            _client = None
        return True
    else:
        return False
