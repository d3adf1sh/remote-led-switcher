from device import led
from . import mqtt


def get_status():
    return led.get_status()


def on():
    status = led.on()
    mqtt.publish("LED", "ON")
    return status


def off():
    status = led.off()
    mqtt.publish("LED", "OFF")
    return status
