from lib.microdot import Microdot, send_file

web_app = Microdot()

@web_app.route('/')
async def index(request):
    return send_file('apps/public/index.html')

@web_app.route('/<path:path>')
async def static(request, path):
    if '..' in path:
       return 'Not found', 404
    return send_file('apps/public/' + path)
