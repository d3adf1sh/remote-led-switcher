from lib.microdot import Microdot
from services import led

led_app = Microdot()

@led_app.get('')
async def get_status(request):
    status = led.get_status()
    return {'status': status}


@led_app.post('/on')
async def on(request):
    status = led.on()
    return {'status': status}


@led_app.post('/off')
async def off(request):
    status = led.off()
    return {'status': status}
