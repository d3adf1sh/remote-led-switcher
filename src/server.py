from lib.microdot import Microdot
from apps import led, web

_server = Microdot()
_server.mount(led.led_app, url_prefix='/led')
_server.mount(web.web_app)

def start(port, debug):
    _server.run(port=port, debug=debug)
