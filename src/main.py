import json
from device import wifi
from services import mqtt
import server


def _connect_to_wifi(settings):
    try:
        wifi_settings = settings['wifi']
        ssid = wifi_settings['ssid']
        password = wifi_settings['password']
        timeout = int(wifi_settings['timeout'])
    except (KeyError, ValueError):
        ssid = ''
        password = ''
        timeout = 0
    if ssid and password:
        if wifi.connect(ssid, password, timeout):
            print('Connected to wifi...')
            return True
        else:
            print('Not connected to wifi.')
    else:
        print('Invalid wifi settings.')


def _connect_to_mqtt(settings):
    try:
        mqtt_settings = settings['mqtt']
        server = mqtt_settings['server']
        port = int(mqtt_settings['port'])
        user = mqtt_settings['user']
        password = mqtt_settings['password']
        ssl = bool(mqtt_settings['ssl'])
    except (KeyError, ValueError) :
        server = ''
        port = 0
        user = ''
        password = ''
        ssl = False
    if server and port > 0:
        if mqtt.connect(server, port, user, password, ssl):
            print("Connected to mqtt service...");
            return True
        else:
            print('Not connected to mqtt service.')
    else:
        print('Invalid mqtt settings.')


def _start_server(settings):
    try:
        server_settings = settings['server']
        port = int(server_settings['port'])
    except (KeyError, ValueError) as error:
        port = 0
    if port > 0:
        print(f'Server running on http://{wifi.get_ip_address()}:{port}.')
        server.start(port, False)
        return True
    else:
        print('Invalid server settings.')


def main():
    with open('settings.json', 'r') as settings_file:
        settings = json.load(settings_file)
    if settings is not None:
        if _connect_to_wifi(settings) and _connect_to_mqtt(settings):
            _start_server(settings)
    else:
        print('Invalid settings file.')


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
