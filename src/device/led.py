from machine import Pin


def _get_pin():
    return Pin("LED", Pin.OUT)


def get_status():
    return _get_pin().value()


def on():
    pin = _get_pin()
    pin.on()
    return pin.value()


def off():
    pin = _get_pin()
    pin.off()
    return pin.value()
