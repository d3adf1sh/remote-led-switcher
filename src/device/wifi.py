from network import WLAN, STA_IF
from time import sleep

_STATUS_DISCONNECTED = 0
_STATUS_CONNECTED = 3


def connect(ssid, password, timeout):
    wifi = WLAN(STA_IF)
    wifi.active(True)
    wifi.connect(ssid, password)
    has_timeout = False
    time_elapsed = 1
    while (wifi.status() >= _STATUS_DISCONNECTED and wifi.status() < _STATUS_CONNECTED) and not has_timeout:
        if timeout >= 0 and time_elapsed > timeout:
            has_timeout = True
        else:
            time_elapsed += 1
            sleep(1)
    return wifi.status() == _STATUS_CONNECTED


def is_connected():
    wifi = WLAN(STA_IF)
    return wifi.status() == _STATUS_CONNECTED


def get_ip_address():
    wifi = WLAN(STA_IF)
    return wifi.ifconfig()[0]


def disconnect(timeout):
    wifi = WLAN(STA_IF)
    wifi.disconnect()
    has_timeout = False
    time_elapsed = 1
    while wifi.status() != _STATUS_DISCONNECTED and not has_timeout:
        if timeout >= 0 and time_elapsed > timeout:
            has_timeout = True
        else:
            time_elapsed += 1
            sleep(1)
    wifi.active(False)
    return wifi.status() == _STATUS_DISCONNECTED
